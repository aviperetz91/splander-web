import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import { LocationOn, ChatBubble, CheckCircle, Cancel, Help, CalendarToday, } from '@material-ui/icons';
import axios from 'axios';
import moment from 'moment';
import { API } from '../../config';
import AddToCalendar from 'react-add-to-calendar';

import 'moment/locale/he';
const DATE_FORMAT = 'dddd, DD בMMMM';
export const HOUR_FORMAT = 'H:mm';

class Receive extends Component {

    // constructor(props) {
    //     super(props);
    //     moment.locale('he')
    //     this.state = {
    //         event: null,
    //         attend: 0,
    //         message: '',
    //         phoneNumber: '',
    //         showMore: false,
    //         isPhoneValid: false,
    //         leavePhoneInput: false,
    //         saveResponseSuccess: false,
    //     }
    //     this.eventId = window.location.pathname.replace('/', '');
    // }

    // componentDidMount = async () => {
    //     const event = await axios.get(`${API}/events/${this.eventId}.json`);
    //     this.setState({ event: event.data }, () => console.log(this.state.event))
    // }

    render() {
        // const { showMore, event } = this.state;

        return (
            <div className="d-flex justify-content-center align-items-center text-white" style={{ height: '100%' }}>
                <div>Welcom To Splander Landing Page</div>
            </div>
        )

        // if (event) {
        //     return (
        //         <div style={{ height: '100%' }} className="mx-auto col-md-8 p-3">
        //             <div style={{ backgroundColor: '#3D3BEE' }}>
        //                 <div className="text-right text-white" style={{ fontSize: '21px' }}>SPLANDER</div>
        //                 <div className="align-items-center">
        //                     <div className="text-white" style={{ fontSize: '18px' }}>Your Social Life Planner</div>
        //                     <div className="text-white mt-3">
        //                         {`${moment(event.startDate).format(DATE_FORMAT)}, ${moment(event.startDate).format(HOUR_FORMAT)} - ${moment(event.endDate).format(HOUR_FORMAT)}`}
        //                     </div>
        //                     <div style={{ border: '2px dashed black', borderRadius: '15px' }} className="card">
        //                         {this.displayEventDetails()}
        //                         {this.displayResponseButtonGroup()}
        //                         {showMore ? this.displayEnterMoreDetails() : null}
        //                     </div>
        //                 </div>
        //             </div>

        //             {this.showSuccessPopup()}
        //             {this.showParticipantsPopup()}

        //             <div className="text-center text-white font-weight-bold mt-3">
        //                 !SPLANDER - Make your life splendid
        //             </div>
        //         </div>
        //     )
        // } else {
        //     return (
        //         <div style={{ height: '100%' }} className="d-flex justify-content-center align-items-center">
        //             <div className="spinner-border text-light" role="status">
        //                 <span className="sr-only">Loading...</span>
        //             </div>
        //         </div>
        //     )
        // }
    }

    // displayEventDetails = () => {
    //     const { event } = this.state;
    //     return (
    //         <div className="p-2">
    //             <div className="text-muted">קיבלת הזמנה לפגישה מ:</div>
    //             <div className="d-flex align-items-center mt-2">
    //                 <Avatar style={{ height: 55, width: 55, backgroundColor: this.getRandomColor() }} alt="" src="/static/images/avatar/1.jpg"></Avatar>
    //                 <div className="ml-2">{event.creator.contact.phoneNumber}</div>
    //             </div>
    //             <div style={{ fontSize: '110%' }} className="font-weight-bold mt-2">{event.title}</div>
    //             <div className="d-flex justify-content-between align-items-center">
    //                 <div>
    //                     <div className="d-flex align-items-center mt-2">
    //                         <LocationOn style={{ color: '#888' }} />
    //                         <div className="ml-2">{event.location}</div>
    //                     </div>
    //                     <div className="d-flex align-items-center mt-1">
    //                         <ChatBubble style={{ color: '#888' }} />
    //                         <div className="ml-2">{event.comment}</div>
    //                     </div>
    //                 </div>
    //                 <div data-toggle='modal' data-target='#showParticipantsPopup' className="d-flex align-items-center p-3">
    //                     {this.renderContacts()}
    //                     {event.contacts.length > 3 ?
    //                         <button
    //                             style={{ border: 'none', backgroundColor: 'white', height: 45, width: 45, borderRadius: 45, marginLeft: '-10px', fontSize: '25px', outline: 'none' }}
    //                             data-toggle='modal'
    //                             data-target='#showParticipantsPopup'
    //                         >
    //                             <Avatar style={{}} alt="+" src="/static/images/avatar/1.jpg">+</Avatar>
    //                         </button>
    //                         : null}
    //                 </div>
    //             </div>
    //         </div>
    //     )
    // }

    // renderContacts = () => {
    //     const { event } = this.state;
    //     let contactsToRender;
    //     if (event.contacts.length < 3 || event.contacts.length === 3) {
    //         contactsToRender = event.contacts.map((contact) => (
    //             <Avatar key={contact.contact.phoneNumber} style={{ height: 50, width: 50, marginLeft: '-10px', backgroundColor: this.getRandomColor() }} alt="" src="/static/images/avatar/1.jpg"></Avatar>
    //         ))
    //     } else {
    //         contactsToRender = event.contacts.map((contact, index) => {
    //             if (index < 2) {
    //                 return (
    //                     <Avatar key={contact.contact.phoneNumber} style={{ height: 50, width: 50, marginLeft: '-10px', backgroundColor: this.getRandomColor() }} alt="" src="/static/images/avatar/1.jpg"></Avatar>
    //                 )
    //             }
    //         })
    //     }
    //     return contactsToRender
    // }

    // displayResponseButtonGroup = () => {
    //     const { attend } = this.state;
    //     return (
    //         <div className="d-flex justify-content-center align-items-center" style={{ border: '1px solid #E2E9F2', borderBottomLeftRadius: '25px', borderBottomRightRadius: '25px' }}>
    //             <div
    //                 className="col" style={{ padding: '6px', cursor: 'pointer' }}
    //                 onClick={() => this.setState({ attend: 1, showMore: true })}
    //             >
    //                 <div
    //                     className="d-flex justify-content-center"
    //                     style={attend === 1 ? { backgroundColor: '#E1EDFB', color: '#3D3BEE', padding: '4px', fontWeight: 'bold', borderRadius: '3px' } : { padding: '4px' }}
    //                 >
    //                     <div className="mr-1"><CheckCircle style={{ backgroundColor: 'white', borderRadius: 50, color: '#44DB5E' }} /></div>
    //                     <div style={{ paddingTop: "2px" }}>אישור</div>
    //                 </div>
    //             </div>
    //             <div
    //                 className="col" style={{ padding: '6px', cursor: 'pointer', borderLeft: '1px solid #E2E9F2', borderRight: '1px solid #E2E9F2' }}
    //                 onClick={() => this.setState({ attend: 2, showMore: true })}
    //             >
    //                 <div
    //                     className="d-flex justify-content-center"
    //                     style={attend === 2 ? { backgroundColor: '#E1EDFB', color: '#3D3BEE', padding: '4px', fontWeight: 'bold', borderRadius: '3px' } : { padding: '4px' }}
    //                 >
    //                     <div className="mr-1"><Cancel style={{ backgroundColor: 'white', borderRadius: 50, color: '#FF2157' }} /></div>
    //                     <div style={{ paddingTop: "2px" }}>דחייה</div>
    //                 </div>
    //             </div>
    //             <div
    //                 className="col" style={{ padding: '6px', cursor: 'pointer' }}
    //                 onClick={() => this.setState({ attend: 3, showMore: true })}
    //             >
    //                 <div
    //                     className="d-flex justify-content-center"
    //                     style={attend === 3 ? { backgroundColor: '#E1EDFB', color: '#3D3BEE', padding: '4px', fontWeight: 'bold', borderRadius: '3px' } : { padding: '4px' }}
    //                 >
    //                     <div className="mr-1"><Help style={{ backgroundColor: 'white', borderRadius: 50, color: '#F5A623' }} /></div>
    //                     <div style={{ paddingTop: "2px" }}>אולי</div>
    //                 </div>
    //             </div>
    //         </div>
    //     )
    // }

    // displayEnterMoreDetails = () => {
    //     const { isPhoneValid, leavePhoneInput } = this.state;
    //     return (
    //         <div>
    //             <div className="p-3 " style={{ border: '1px solid #E2E9F2' }}>
    //                 <input
    //                     type="text"
    //                     style={{ direction: 'rtl', width: '100%', border: 'none', outline: 'none', borderRadius: '20px' }}
    //                     placeholder="כתוב כאן את ההערה שלך"
    //                     onChange={(event) => this.setState({ message: event.target.value })}
    //                 />
    //             </div>
    //             <div>
    //                 <div className="mt-3 mb-1 mx-auto d-flex justify-content-center" style={{ width: '60%' }}>
    //                     <TextField
    //                         id="outlined-basic"
    //                         label="הזן מספר פלאפון לאימות"
    //                         variant="outlined"
    //                         onChange={(event) => this.setState({ leavePhoneInput: false })}
    //                         onBlur={(event) => this.phoneInputHandler(event)}
    //                     />
    //                 </div>
    //                 <div className="mx-auto" style={{ color: '#dc3545', fontSize: '80%', fontWeight: 'bold', width: '60%' }}>
    //                     {!isPhoneValid && leavePhoneInput ? 'אנא הזן מספר פלאפון חוקי' : null}
    //                 </div>
    //             </div>
    //             <div className="d-flex justify-content-end">
    //                 <button
    //                     className="d-flex mr-2 mt-3 mb-2 bg-white border-0"
    //                     style={{ fontSize: 15, fontWeight: 'bold', color: '#3D3BEE', cursor: 'pointer', outline: 'none' }}
    //                     onClick={this.saveHandler}
    //                     data-toggle={isPhoneValid ? `modal` : ''}
    //                     data-target={isPhoneValid ? '#success' : ''}
    //                 >
    //                     שמירה
    //             </button>
    //             </div>
    //         </div>
    //     )
    // }

    // showSuccessPopup = () => {
    //     const { saveResponseSuccess, event } = this.state;
    //     return (
    //         <div className="modal fade" id="success" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    //             <div className="modal-dialog modal-dialog-centered">
    //                 <div className="modal-content">
    //                     {saveResponseSuccess ?
    //                         <div>
    //                             <div className="modal-header border-0">
    //                                 <h5 className="modal-title font-weight-bold" id="exampleModalLabel">תגובתך נרשמה בהצלחה!</h5>
    //                             </div>
    //                             <div className="modal-body py-0">
    //                                 <div className="d-flex">
    //                                     <CalendarToday />  &nbsp;
    //                                     <AddToCalendar
    //                                         event={{
    //                                             title: event.title,
    //                                             description: event.comment,
    //                                             location: event.location,
    //                                             startTime: event.startDate,
    //                                             endTime: event.endDate
    //                                         }}
    //                                         buttonLabel="לחץ לשמירה של האירוע ביומן"
    //                                     />
    //                                 </div>
    //                                 <div className="mt-2">תרצה/י להוריד את האפליקציה ולהקל לעצמך את ניהול החיים?</div>
    //                             </div>
    //                             <div className="modal-footer border-0">
    //                                 <button type="button" className="btn" style={{ backgroundColor: '#3D3BEE', color: 'white' }}>קללל, תוריד</button>
    //                                 <button type="button" className="btn" style={{ border: '1px solid #3D3BEE', color: '#3D3BEE' }} data-dismiss="modal">פעם אחרת</button>
    //                             </div>
    //                         </div>
    //                         :
    //                         <div style={{ height: '100%' }} className="d-flex justify-content-center align-items-center p-5">
    //                             <div className="spinner-border text-primary" role="status">
    //                                 <span className="sr-only">Loading...</span>
    //                             </div>
    //                         </div>
    //                     }
    //                 </div>
    //             </div>
    //         </div>
    //     )
    // }

    // showParticipantsPopup = () => {
    //     const { event } = this.state;
    //     return (
    //         <div className="modal fade" id="showParticipantsPopup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    //             <div className="modal-dialog modal-dialog-centered">
    //                 <div className="modal-content">
    //                     <div className="modal-header border-0">
    //                         <h5 className="modal-title font-weight-bold" id="exampleModalLabel">כל המוזמנים</h5>
    //                         <button type="button" className="close" data-dismiss="modal" aria-label="Close">
    //                             <span aria-hidden="true">&times;</span>
    //                         </button>
    //                     </div>
    //                     <div className="modal-body py-0">
    //                         {event.contacts.map((contact, index) => (
    //                             <div key={contact.contact.phoneNumber} className="d-flex justify-content-start align-items-center mt-2">
    //                                 <Avatar style={{ height: 50, width: 50, backgroundColor: this.getRandomColor() }} alt="" src="/static/images/avatar/1.jpg">

    //                                 </Avatar>
    //                                 <div className="ml-2" style={{ position: 'relative' }}>
    //                                     {event.contacts[index].attend === 2 ?
    //                                         <Cancel
    //                                             style={{
    //                                                 fontSize: 20,
    //                                                 backgroundColor: 'white',
    //                                                 color: '#FF2157',
    //                                                 borderRadius: 50,
    //                                                 position: 'absolute',
    //                                                 right: -25,
    //                                                 top: 27
    //                                             }} />
    //                                         : event.contacts[index].attend === 1 ?
    //                                             <CheckCircle
    //                                                 style={{
    //                                                     fontSize: 20,
    //                                                     backgroundColor: 'white',
    //                                                     color: '#44DB5E',
    //                                                     borderRadius: 50,
    //                                                     position: 'absolute',
    //                                                     right: -25,
    //                                                     top: 27
    //                                                 }} />
    //                                             : event.contacts[index].attend === 3 ?
    //                                                 <Help
    //                                                     style={{
    //                                                         fontSize: 20,
    //                                                         backgroundColor: 'white',
    //                                                         color: '#F5A623',
    //                                                         borderRadius: 50,
    //                                                         position: 'absolute',
    //                                                         right: -25,
    //                                                         top: 27
    //                                                     }} />
    //                                                 : null}
    //                                     <div>{contact.contact.phoneNumber}</div>
    //                                     <div className="text-muted">{contact.comment}</div>
    //                                 </div>
    //                             </div>
    //                         ))}
    //                     </div>
    //                     <div className="modal-footer border-0"></div>
    //                 </div>
    //             </div>
    //         </div>
    //     )
    // }

    // getRandomColor = () => {
    //     let r, g, b;
    //     r = Math.floor(Math.random() * 255);
    //     g = Math.floor(Math.random() * 255);
    //     b = Math.floor(Math.random() * 255);
    //     return `rgb(${r}, ${g}, ${b})`
    // }

    // phoneInputHandler = (event) => {
    //     const isValid = this.validatePhoneNumber(event.target.value)
    //     this.setState({
    //         phoneNumber: event.target.value,
    //         leavePhoneInput: true,
    //         isPhoneValid: isValid
    //     });
    // }

    // saveHandler = async () => {
    //     const { event, isPhoneValid, phoneNumber, attend, message } = this.state;
    //     if (isPhoneValid) {
    //         let phone = '+972'
    //         for (let i = 1; i < phoneNumber.length; i++) {
    //             phone += phoneNumber[i]
    //         }
    //         const contactIndex = event.contacts.findIndex(contact => contact.contact.phoneNumber === phone)
    //         if (contactIndex !== -1) {
    //             const contact = event.contacts[contactIndex];
    //             contact.attend = attend;
    //             contact.comment = message;
    //             const res = await axios.put(`${API}/events/${this.eventId}/contacts/${contactIndex}.json`, contact)
    //             if (res) {
    //                 this.setState({ saveResponseSuccess: true })
    //             }
    //         }
    //         else {
    //             console.log("ERROR")
    //         }
    //     }
    // }

    // validatePhoneNumber = (phone) => {
    //     const re = /^[0][5][0|2|3|4|5|9]{1}[-]{0,1}[0-9]{7}$/g;
    //     return re.test(String(phone));
    // }
}

export default Receive;


