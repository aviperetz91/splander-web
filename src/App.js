import './App.css';
import React, { Component } from 'react';
import Receive from './screens/Receive/Receive'

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
           WELLCOM TO SPLANDER WEB PAGE!
          </p>        
        </header> */}
        <Receive />
      </div>
    );
  }
}

export default App;
